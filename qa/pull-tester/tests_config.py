#!/usr/bin/env python3
# Copyright (c) 2013-2016 The Methuselah Core developers
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

SRCDIR="/home/squbs/methuselah"
BUILDDIR="/home/squbs/methuselah"
EXEEXT=""

# These will turn into comments if they were disabled when configuring.
ENABLE_WALLET=1
ENABLE_UTILS=1
ENABLE_METHUSELAHD=1
ENABLE_ZMQ=1
