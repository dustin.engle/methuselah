
![Alt text](doc/methuselah.png)


## METHUSELAH

#### SegWit and SegWit2x active, Lightning Network compatible, Atomic Swap compatible and an increased non-SegWit block size of 4MB;  introducing the first Masternode-enabled digital currency based off core Bitcoin version 0.14.2.  METHUSELAH endeavours to eliminate unwanted transaction malleability through SegWit activation from day 0; solves capacity constraints suffered by Bitcoin and its peer group through a larger non-SegWit block size and increased signature operations - effectively introducing SegWit4x; is Lightning Network compatible - supporting instant payments, scalability, low cost and cross blockchain transactions; proposes a fair distribution through a Zero Start Instamine Protection (ZSIP) policy and Zero Start Masternode Reward (ZSMR) policy; introduces a new unique Masternode payment re-balancing algorithm, labelled Reactive Equilibria (REV1); and furthermore, implements a modified version of the new effective D106 difficulty re-targeting algorithm


__________________________________________________________________________


###### Block Size: 4 MB
###### Max Block Size Serialized Size: 16 MB
###### Proof-of-Work Algorithm: Lyra2rev2
###### Block Time: ~60 seconds
###### Coin Maturity: 100 blocks
###### Transactions Per Second: 133 (Bitcoin: 3, Bitcoin Cash: 27, Dash: 13)
###### Transactions Per 24 hours: 11,520,000
###### Difficulty Retargeting: New D106 Algorithm
###### Maximum Coin Supply: 150 Million
###### SegWit: Active
###### SegWit2x: Active
###### SegWit4x: Active
###### Lightning Network: Compatible
###### Atomic Swaps: Compatible
###### Masternodes: Enabled
###### Masternodes Collateral: 9,600 SAP
###### Masternode Reward: Rebalancing via New Reactive Equilibria (REV1) Algorithm
###### ZSMR: Explicit Masternode Decentralisation Promotion [Zero Reward to Block 1921]
###### ZSIP: Zero Start Instamine Protection [7 Days: 10080 Blocks]


__________________________________________________________________________


## Introducing METHUSELAH

Introducing a new decentralised, open source, community driven digital currency, focusing on e-commerce utility.

The name is a Danish word that has never been used in any cryptocurrency context until now. It means quick/immediate - referring to the focus that we have on ensuring that transactions are as fast as can be and to be the most efficient cryptocurrency for e-commerce utility in the sector.  The currency unit of account is **SAP**, along with its exchange ticker.


##### E-Commerce
Global e-commerce sales are projected to reach $2.2 trillion for 2017 and to increase two-fold by 2021 to $4.4 trillion. METHUSELAH seeks to position itself as a competitive altcoin for e-commerce application. This will be achieved through developing and implementing relevant technology that facilitates e-commerce transactions as smoothly as possible. We will always strive to create partnerships and increase real-world utilisation.


##### Open Source
METHUSELAH is open source. Anyone is free to contribute to METHUSELAH’ development and make a pull request through Github. Further details, making community contributions as smooth as possible, will become available in the future on the new website.


##### Decentralized Ownership
Ownership and access rights are decentralized. This ensures that the long-term application and longevity is not dependent on a single individual any longer. This safeguards not only METHUSELAH' future, but also your investment and partnerships.


##### Community Orientated
One of METHUSELAH core values is being community oriented. Rest assured knowing that the METHUSELAH foundation team will always strive to collect feedback and suggestions from the community and evaluate the input.

With the upcoming METHUSELAH-vote you will be able to not only propose ideas publicly to all METHUSELAH holders but also vote on what needs developing or prioritising next. Look out for METHUSELAH-vote on the Roadmap.


##### Speed & Privacy
Speed and privacy are core concerns for users today as well as e-commerce businesses. To ensure that METHUSELAH is as effective as possible for e-commerce application, METHUSELAH will maintain a high focus on increasing anonymity and most important on increasing speed of transactions - to always be at the forefront.


### Links

- **[METHUSELAH Website](https://methuselah.io)**
- **[METHUSELAH Explorer](https://methuselah.info)**
- **[METHUSELAH Discord](https://discord.gg/5gzvadZ)**
- **[METHUSELAH Reddit](https://www.reddit.com/r/METHUSELAHproject)**
- **[METHUSELAH Twitter](https://twitter.com/METHUSELAHproject)**
- **[METHUSELAH Telegram](https://t.me/joinchat/AAAAAFNgas-8mT4fmKulFg)**
- **[METHUSELAH Facebook](https://www.facebook.com/METHUSELAHproject)**
- **[METHUSELAH Community News](https://methuselah.news)**


### METHUSELAH Foundation Team

- **Squbs (squbs@methuselah.io) - Lead Developer & Business Development**
- **Cryptovore (dan@methuselah.io) - Lead Marketing & PR, Business Development**
- **Ekam (ekam@methuselah.io) - Lead Developer & Business Development**
- **John Smith (john@methuselah.io) - Marketing & PR, Business Development**
- **Kasper (kasper@methuselah.io) - Developer, Marketing & PR**
- **Bruno (bruno@methuselah.io) - Developer**
- **Andrew (andrew@methuselah.io) - Developer**
- **Flydancer (flydancer@methuselah.io) - Developer**
- **Proximus (proximus@methuselah.io) - Marketing & PR**


__________________________________________________________________________


### METHUSELAH Comparison with Leading Digital Currencies

There is only **one** digital currency on this list that is designed - from the bottom up - to be scalable, fast, low cost, secure, decentralized and provide for instant utility, and that is **METHUSELAH**, the alternative conclusion is simply an escalation of commitment.  As part of METHUSELAH' decentralization manifesto, we fully support a diverse crypto-currency sector - with the caveat that we are **strictly against the concentration of hashing power**, through the use of asic hardware.

> Escalation of commitment refers to a pattern of behavior in which an individual or group will continue to rationalize their decisions, actions, and investments when faced with increasingly negative outcomes rather than alter their course.

![Alt text](doc/methuselah_compare.png)


__________________________________________________________________________


### Specifications

As above summary, in addition, the following notable items:


#### Coinbase Transactions, Pool Mining and Wallet Mining

**IMPORTANT**

Other than 'real world' testing of lightning transactions and cross chain atomic swaps, **all METHUSELAH' features are enabled and work from day 0**; end-users can utilise METHUSELAH' speed, larger chain capacity, SegWit and Masternode functionality right now.

Due to the unique combination of SegWit, miner reward and Masternode payments, as of this writing, some mining software and various pools do not yet fully support METHUSELAH' coinbasetxn configuration.  GPU mining does work, but pool and/or mining software may need further code changes for SegWit blocks.

##### Wallet Mining
```
Approach 1 (Local):
(1) Start methuselahd/methuselah-qt locally
(2) Run: "./methuselah-cli generate 1" (or via debug console in the Qt gui wallet)
```
```
Approach 2 (Remote):
(1) Start methuselahd on remote server and local server
(2) On your local wallet (server) run: "./methuselah-cli getaccountaddress 0"
(3) On Your remote server Run: "./methuselah-cli generatetoaddress 1 <address from step2>"
(4) Shutdown local wallet (server)
```

More information can be found on [METHUSELAH' Knowledge Base](https://kb.methuselah.info/)

##### GPU Mining

Please follow [METHUSELAH' Knowledge Base GPU mining guide](https://kb.methuselah.info/kb/5a1a52ed48e3b4777551b314)


#### Zero Start Instamine Protection (ZSIP)

METHUSELAH blockchain has been set to issue a 0 block subsidy up to **block height 10080**.  At 640 blocks per day, this is approximately 7 days. During this time, community members will be informed of launch and will be able to configure their setups for mining.  


#### Maximum Coin Supply and Block Reward

METHUSELAH will be capped at a maxium supply of **150 million coins**.  This target supports a managed inflation rate that allows for a constant block reward of **10 SAPs**.

The constant block reward prevents event cliffs related to expected "halvings" and will support a more consistent and stable participation rate over a long time horizon.

More information will be available on the METHUSELAH' [website](https://methuselah.io)

##### The Monetary Curve Based on a 10 SAP Block Reward
![Alt text](doc/mcurve.png)


##### The Monetary Curve Underlying Dataset with Zero Start Block Offset
![Alt text](doc/monetary_curve_table.png)


#### POW Algorithm: Lyra2REv2

Fast, power-efficient and asic resistant algorithm.  The METHUSELAH development team will continue to support a decentralized asic resistant blockchain, and will update the POW algorithm if the latter is no longer the case.

Lyra2 is simple tunable password hashing scheme (PHS) based on cryptographic sponges.  Designed to be resistant against parallel attacks from mult-core/gpu systems through its sequential processing.  More specific information about Lyra2 itself can be obtained from: http://lyra-2.net/

Lyra2RE wrapped the Lyra2 PHS with additional cryptographic hash functions, and Lyra2REv2 is another derivative, again, with Lyra2 at its core.  Notably, chaining the hashing algorithms allows for an easy path to fulfil a decentralization ideology and maintain asic resistance.

The difference between Lyra2RE and Lyra2REv2 is as follows;

- Additional 2 rounds of [CubeHash](https://en.wikipedia.org/wiki/CubeHash)
- Additional 1 round of [Blue Midnight Wish](https://www.mathematik.hu-berlin.de/~schliebn/dl/Blue-Midnight-Wish.pdf)
- Removal of [Groestl](http://www.groestl.info/)


#### **NEW** Difficulty Retargeting Algorithm (D106 - Amaury Sechet)

Recent public discussions on how to address Bitcoin Cash's hash rate oscillation has resulted in an excellent approach that addresses many weaknesses of various other algorithms.  A well developed analsysis was presented by Bitcoin Cash's developer Amaury Sechet and supported by Zawy12 via thorough analysis, with simulated results that demonstate the algorthim's effectiveness.  The proposed implementation was modified specific to METHUSELAH blockchain.

More information can be found here:

- [Bitcoin Mailing List Discussion](https://lists.linuxfoundation.org/pipermail/bitcoin-ml/2017-August/000136.html)
- [Bitcoin News - Bitcoin Cash Hard Fork](https://news.bitcoin.com/bitcoin-cash-hard-fork-plans-updated-new-difficulty-adjustment-algorithm-chosen/)

##### The Modified D106 Algorithm Targeting a 60 Second Block Time for METHUSELAH Blockchain
![Alt text](doc/D106_algorithm_test.png)


#### Masternodes (and ZSMR)

Masternodes will be supported with a collateral requirement of **9,600 SAP**.  

The collateral requirement is a dynamic target value and will be updated for future
releases based on the market price of METHUSELAH and return on investment relative to
its peer group.

Masternode payments do not start until after approximately 5 weeks post launch. More
specifically, after **block height 1921**.  This is an active promotion of Masternode
decentralization via the Zero Start Masternode Reward policy.  This delayed reward
mechanism seeks to provide sufficient time for swap holders and investors alike to set
up nodes.

Once the exact collateral requirement is accumulated inside an applicable wallet, the
associated configured Masternode can be activated.  Note that the node will only start
receiving payments after the aforementioned ZSMR block height.


#### Masternode Payment Rebalancing: **NEW** Reactive Equilibria (REV1)

Unique to METHUSELAH is the use of a new continuous activation function based algorithm,
labelled Reactive Equilibria (REV1), that seeks to rebalance the payments made
to miners versus Masternodes.  There is a hard limit of 60% of the block reward that
can be appropriated for Masternode payments; this limit defines the boundary at which
total supply locked by Masternodes versus total circulation remains below ~13%.
As the Masternode count increases, the payment to Masternode holders will
decline in favour of miners in order to support a decentralised ideology.  The
new algorithm is simple in design but highly effective and lays the foundation for
future advancements.  More information about this feature will be posted on the
[main site](https://methuselah.io).

##### Reactive Equilibria Masternode Payment Rebalancing Profile
![Alt text](doc/reactive_equilibria.png)


#### **NEW** Segregated Witness 2x/4x

![Alt text](doc/segwit.png)

SegWit2x is a combination of both SegWit and a 2MB hardfork. METHUSELAH has further
increased the non-SegWit block size to **4MB** from the outset to allow for greater
scalability and utility.  The maximum serialised block size is **16MB** for METHUSELAH.

METHUSELAH is both lightning network and atomic swap compatible. And unlike
many other "compatible" altcoins, SegWit, SegWit2x and the larger blocksize are enabled from the outset.

For further information about Segregated Witness please visit [bitcoincore.org](https://bitcoincore.org/en/2016/01/26/SegWit-benefits/)

##### Segregated Witness Transaction on METHUSELAH Testnet, [block:6635, txid:3fc630ac1a4b91714d7c7150275b8be019152329e70d1f7a37240a7331b9fab6]
![Alt text](doc/testnet_segwit.png)


#### InstantTX and DarkSend Removal
*(Reproduced here for information purposes only, not release related)*

Dropped support for both InstantTX and DarkSend. With the SegWit upgrade these features are going to be superseeded by far superior technology. SegWit will enable the METHUSELAH to adopt the [Lightning Network](https://lightning.network/lightning-network-paper.pdf), cross-chain atomic swaps, advanced versions of [TumbleBit](https://eprint.iacr.org/2016/575.pdf) and more.


#### Hierarchical Deterministic Key Generation
*(Reproduced here for information purposes only, not release related)*

Newly created wallets will use hierarchical deterministic key generation
according to BIP32 (keypath m/0'/0'/k').
Existing wallets will still use traditional key generation.

Backups of HD wallets, regardless of when they have been created, can
therefore be used to re-generate all possible private keys, even the
ones which haven't already been generated during the time of the backup.
**Attention:** Encrypting the wallet will create a new seed which requires
a new backup!

Wallet dumps (created using the `dumpwallet` RPC) will contain the deterministic
seed. This is expected to allow future versions to import the seed and all
associated funds, but this is not yet implemented.

HD key generation for new wallets can be disabled by `-usehd=0`. Keep in
mind that this flag only has affect on newly created wallets.
You can't disable HD key generation once you have created a HD wallet.

There is no distinction between internal (change) and external keys.

HD wallets are incompatible with older versions of Bitcoin Core.

[Pull request](https://github.com/bitcoin/bitcoin/pull/8035/files), [BIP 32](https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki)


#### Signature validation using libsecp256k1
*(Reproduced here for information purposes only, not release related)*

ECDSA signatures inside Bitcoin transactions now use validation using
[libsecp256k1](https://github.com/bitcoin-core/secp256k1) instead of OpenSSL.

Depending on the platform, this means a significant speedup for raw signature
validation speed. The advantage is largest on x86_64, where validation is over
five times faster. In practice, this translates to a raw reindexing and new
block validation times that are less than half of what it was before.

Libsecp256k1 has undergone very extensive testing and validation.

A side effect of this change is that libconsensus no longer depends on OpenSSL.


#### Direct headers announcement (BIP 130)
*(Reproduced here for information purposes only, not release related)*

Between compatible peers, [BIP 130](https://github.com/bitcoin/bips/blob/master/bip-0130.mediawiki)
direct headers announcement is used. This means that blocks are advertised by
announcing their headers directly, instead of just announcing the hash. In a
reorganization, all new headers are sent, instead of just the new tip. This
can often prevent an extra roundtrip before the actual block is downloaded.


__________________________________________________________________________


### Acknowledgements

Credit goes to Bitcoin Core, Dash and Bitsend for providing a basic platform for
METHUSELAH to enhance and develop, in concert with a shared desire to support the
adoption of a decentralised digital currency future for the masses.


__________________________________________________________________________


### License

METHUSELAH Core is released under the terms of the MIT license. See [COPYING](COPYING) for more
information or see https://opensource.org/licenses/MIT.


__________________________________________________________________________


### Development Process

The `master` branch is meant to be stable. Development is normally done in separate branches.
[Tags](https://github.com/methuselah/methuselah/tags) are created to indicate new official,
stable release versions of METHUSELAH' Core.

The contribution workflow is described in [CONTRIBUTING.md](CONTRIBUTING.md).


__________________________________________________________________________


### Building process

**compiling METHUSELAH from git**

Use the autogen script to prepare the build environment.

    ./autogen.sh
    ./configure
    make

**precompiled binaries**

Precompiled binaries are available at GitHub, see
https://github.com/methuselah/methuselah/releases

Always verify the signatures and checksums.


__________________________________________________________________________
