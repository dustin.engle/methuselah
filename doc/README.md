METHUSELAH
======

##### Copyright (c) 2009-2017 METHUSELAH Developers
##### Copyright (c) 2014-2015 Dash Developers
##### Copyright (c) 2017-2018 METHUSELAH Developers

Setup
---------------------
[METHUSELAH Core](https://github.com/squbs/methuselah/releases) is the original METHUSELAH client and it builds the backbone of the network. However, it downloads and stores the entire history of METHUSELAH transactions; depending on the speed of your computer and network connection, the synchronization process can take anywhere from a few minutes to hours or a day or more. Thankfully you only have to do this once.

### Need Help?

* See the documentation at the [METHUSELAH Knowledge Base](https://kb.methuselah.info)
for help and more information.
* Ask for help on [METHUSELAH Discord](https://discord.gg/5gzvadZ)


Building
---------------------
The following are developer notes on how to build METHUSELAH on your native platform. They are not complete guides, but include notes on the necessary libraries, compile flags, etc.

- [OSX Build Notes](build-osx.md)
- [Unix Build Notes](build-unix.md)
- [Windows Build Notes](build-msw.md)

License
---------------------
Distributed under the [MIT/X11 software license](http://www.opensource.org/licenses/mit-license.php).
This product includes software developed by the OpenSSL Project for use in the [OpenSSL Toolkit](http://www.openssl.org/). This product includes
cryptographic software written by Eric Young ([eay@cryptsoft.com](mailto:eay@cryptsoft.com)), and UPnP software written by Thomas Bernard.
